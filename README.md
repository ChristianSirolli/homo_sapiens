# Homo Sapiens
Adds creatura-based human NPCs.

## Credits
The texture "Eve_by_CSirolli.png" is licensed by CSirolli under CC-BY-SA-3.0, having been adapted from "Sachou205_by_Sachou205.png" and "Crowned3_by_sdzen.png" (both licensed under CC-BY-SA-3.0, the latter by sdzen).

The texture "Sachou205_by_Sachou205.png" is licensed by Sachou205 under CC-BY-SA-3.0.