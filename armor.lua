function homo_sapiens.init_mob_armor(initmob)
	local uuid = initmob.uuid
	if not uuid then
		return false
	end
	local armor_inv = minetest.create_detached_inventory(uuid.."_armor", {
		on_put = function(inv, listname, index, stack, player)
			-- validate_armor_inventory(player)
			armor:save_armor_inventory(player)
			armor:set_player_armor(player)
		end,
		on_take = function(inv, listname, index, stack, player)
			-- validate_armor_inventory(player)
			armor:save_armor_inventory(player)
			armor:set_player_armor(player)
		end,
		on_move = function(inv, from_list, from_index, to_list, to_index, count, player)
			-- validate_armor_inventory(player)
			armor:save_armor_inventory(player)
			armor:set_player_armor(player)
		end,
		allow_put = function(inv, listname, index, put_stack, player)
			if player.uuid ~= uuid then
				return 0
			end
			local element = armor:get_element(put_stack:get_name())
			if not element then
				return 0
			end
			for i = 1, 6 do
				local stack = inv:get_stack("armor", i)
				local def = stack:get_definition() or {}
				if def.groups and def.groups["armor_"..element]
						and i ~= index then
					return 0
				end
			end
			return 1
		end,
		allow_take = function(inv, listname, index, stack, player)
			if player.uuid ~= uuid then
				return 0
			end
			--cursed items cannot be unequiped by the player
			local is_cursed = minetest.get_item_group(stack:get_name(), "cursed") ~= 0
			if not minetest.is_creative_enabled(player) and is_cursed then
				return 0
			end
			return stack:get_count()
		end,
		allow_move = function(inv, from_list, from_index, to_list, to_index, count, player)
			if player.uuid ~= uuid then
				return 0
			end
			return count
		end,
	})
	armor_inv:set_size("armor", 6)
	if not armor:load_armor_inventory(initmob) and armor.migrate_old_inventory then
		local player_inv = initmob:get_inventory()
		player_inv:set_size("armor", 6)
		for i=1, 6 do
			local stack = player_inv:get_stack("armor", i)
			armor_inv:set_stack("armor", i, stack)
		end
		armor:save_armor_inventory(initmob)
		player_inv:set_size("armor", 0)
	end
	for i=1, 6 do
		local stack = armor_inv:get_stack("armor", i)
		if stack:get_count() > 0 then
			armor:run_callbacks("on_equip", initmob, i, stack)
		end
	end
	armor.def[uuid] = {
		init_time = minetest.get_gametime(),
		level = 0,
		state = 0,
		count = 0,
		groups = {},
	}
	for _, phys in pairs(armor.physics) do
		armor.def[uuid][phys] = 1
	end
	for _, attr in pairs(armor.attributes) do
		armor.def[uuid][attr] = 0
	end
	for group, _ in pairs(armor.registered_groups) do
		armor.def[uuid].groups[group] = 0
	end
	local skin = initmob.textures[0]
	armor.textures[uuid] = {
		skin = skin,
		armor = "3d_armor_trans.png",
		wielditem = "3d_armor_trans.png",
		preview = armor.default_skin.."_preview.png",
	}
	-- local texture_path = minetest.get_modpath("player_textures")
	-- if texture_path then
	-- 	local dir_list = minetest.get_dir_list(texture_path.."/textures")
	-- 	for _, fn in pairs(dir_list) do
	-- 		if fn == "player_"..uuid..".png" then
	-- 			armor.textures[uuid].skin = fn
	-- 			break
	-- 		end
	-- 	end
	-- end
	armor:set_player_armor(initmob)
	return true
end