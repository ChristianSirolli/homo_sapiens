name_generator.parse_lines(io.lines(minetest.get_modpath('name_generator').."/data/creatures.cfg"))

homo_sapiens = {
	modpath = minetest.get_modpath('homo_sapiens'),
	male_textures = 'Adam_by_CSirolli.png',
	female_textures = 'Eve_by_CSirolli.png',
	mods = {
		skeletons = minetest.global_exists('skeletons'),
		armor = minetest.global_exists('armor'),
		default = minetest.global_exists('default'),
		skills = minetest.global_exists('skills'),
		clothing = minetest.global_exists('clothing'),
		skinsdb = minetest.global_exists('skinsdb'),
		craft_table = minetest.global_exists('craft_table'),
		i3 = minetest.global_exists('i3'),
	}
}

function homo_sapiens.gen_uuid()
    local template = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'
    return string.gsub(template, '[xy]', function(c)
        local v = (c == 'x') and math.random(0, 0xf) or math.random(8, 0xb)
        return string.format('%x', v)
    end)
end

function homo_sapiens.shared_owner(obj1, obj2)
	if not obj1 or not obj2 then return false end
	obj1 = creatura.is_valid(obj1)
	obj2 = creatura.is_valid(obj2)
	if obj1
	and obj2
	and obj1:get_luaentity()
	and obj2:get_luaentity() then
		obj1 = obj1:get_luaentity()
		obj2 = obj2:get_luaentity()
		return obj1.owner and obj2.owner and obj1.owner == obj2.owner
	end
	return false
end

function homo_sapiens.is_value_in_table(tbl, val)
	for _, v in pairs(tbl) do
		if v == val then
			return true
		end
	end
	return false
end

if homo_sapiens.mods.armor then
	dofile(homo_sapiens.modpath..'/armor.lua')
end

creatura.register_mob('homo_sapiens:human', {
    max_health = 20,
    armor_groups = {fleshy = 10},
    damage = 1,
    speed = 1,
    tracking_range = 24,
    step_height = 1,
    max_fall = 6,
    turn_rate = 7,
    bouyancy_multiplier = 1,
    hydrodynamics_multiplier = 1,
    mesh = 'skinsdb_3d_armor_character_5.b3d',
    hitbox = {
        width = 0.5,
        height = 2
    },
    visual_size = {x = 1, y = 1},
	male_textures = {{homo_sapiens.male_textures, "blank.png", "blank.png", "blank.png"}},
    female_textures = {{homo_sapiens.female_textures, "blank.png", "blank.png", "blank.png"}},
	child_textures = {{'character.png', "blank.png", "blank.png", "blank.png"}},
    animations = {
        stand = {range = {x = 0, y = 79}, speed = 10, frame_blend = 0.3, loop = true},
        sit = {range = {x = 81, y = 160}, speed = 20, frame_blend = 0.3, loop = true},
        lay = {range = {x = 162, y = 166}, speed = 20, frame_blend = 0.3, loop = true},
		walk = {range = {x = 168, y = 187}, speed = 30, frame_blend = 0.3, loop = true},
        mine = {range = {x = 189, y = 198}, speed = 30, frame_blend = 0.3, loop = true},
        walk_mine = {range = {x = 200, y = 219}, speed = 30, frame_blend = 0.3, loop = true},
        -- run = {range = {x = 41, y = 59}, speed = 45, frame_blend = 0.3, loop = true},
		-- wave = {},
		-- point = {},
		
        -- Standard animations.
		-- stand     = {range={x = 0,   y = 79}},
		-- lay       = {range={x = 162, y = 166}, eye_height = 0.3, override_local = true, collisionbox = {-0.6, 0.0, -0.6, 0.6, 0.3, 0.6}},
		-- walk      = {range={x = 168, y = 187}},
		-- mine      = {range={x = 189, y = 198}},
		-- walk_mine = {range={x = 200, y = 219}},
		-- sit       = {range={x = 81,  y = 160}, eye_height = 0.8, override_local = true, collisionbox = {-0.3, 0.0, -0.3, 0.3, 1.0, 0.3}}
	},
    makes_footstep_sound = true,
	catch_with_net = false,
	catch_with_lasso = false,
	assist_owner = true,
	consumable_nodes = {
		["default:apple"] = "air",
		["flowers:mushroom_brown"] = "air",
	},
    follow = {
        'farming:bread',
        'default:apple'
    },
    utility_stack = {
        {
			utility = "animalia:wander",
			step_delay = 0.25,
			get_score = function(self)
				return 0.1, {self}
			end
		},
		{
			utility = "animalia:swim_to_land",
			step_delay = 0.25,
			get_score = function(self)
				if self.in_liquid then
					return 0.3, {self}
				end
				return 0
			end
		},
		{
			utility = 'animalia:attack_target',
			get_score = function(self)
				local order = self.order or 'wander'
				if order ~= 'wander' then return 0 end
				local target = self._target
				if target
				and not homo_sapiens.shared_owner(self, target) then
					return 0.4, {self, target}
				end
				return 0
			end
		},
		{
			utility = 'animalia:stay',
			step_delay = 0.25,
			get_score = function(self)
				local order = self.order or 'wander'
				if order == 'sit' then
					return 0.5, {self}
				end
				return 0
			end
		},
		{
			utility = 'animalia:follow_player',
			get_score = function(self)
				local owner = self.owner and self.order == 'follow' and minetest.get_player_by_name(self.owner)
                local player = owner or creatura.get_nearby_player(self)
				if player and self:follow_wielded_item(player) then
					return 0.6, {self, player, owner}
				end
				return 0
			end
		},
		{
			utility = "animalia:breed",
			step_delay = 0.25,
			get_score = function(self)
				if self.breeding
				and animalia.get_nearby_mate(self, self.name) then
					return 0.5, {self}
				end
				return 0
			end
		},
    },
	activate_func = function(self)
		animalia.initialize_api(self)
		animalia.initialize_lasso(self)
		self.order = self:recall("order") or "wander"
		self.owner = self:recall("owner") or nil
		self.enemies = self:recall("enemies") or {}
		self.uuid = self:recall("uuid") or homo_sapiens.gen_uuid()
        self.name = self:recall('name') or name_generator.generate("human male")..' '..name_generator.generate("human surname")
		self.inventory = minetest.get_inventory({type = 'detached', name = self.uuid..'_inventory'}) or nil
		if homo_sapiens.mods.armor then
			self.armor = minetest.get_inventory({type = 'detached', name = self.uuid..'_armor'}) or nil
		end
		self.clothing = minetest.get_inventory({type = 'detached', name = self.uuid..'_clothing'}) or nil
		if self.owner
		and minetest.get_player_by_name(self.owner) then
			if not homo_sapiens.is_value_in_table(animalia.pets[self.owner], self.object) then
				table.insert(animalia.pets[self.owner], self.object)
			end
		end
		if not self.inventory then
			minetest.create_detached_inventory(self.uuid)
			self.inventory = minetest.get_inventory({type = 'detached', name = self.uuid..'_inventory'})
			self.clothing = minetest.get_inventory({type = 'detached', name = self.uuid..'_clothing'})
			self.inventory:set_size('craft', 9)
			self.inventory:set_size('main', 9)
		end
		if homo_sapiens.mods.armor and not self.armor then
			homo_sapiens.init_mob_armor(self)
		end
	end,
    step_func = function(self)
        animalia.step_timers(self)
        animalia.head_tracking(self, 0.5, 0.75)
        animalia.do_growth(self, 60)
        animalia.update_lasso_effects(self)
    end,
    death_func = function(self)
        if self:get_utility() ~= "animalia:die" then
            self:initiate_utility("animalia:die", self)
        end
    end,
    on_punch = function(self, puncher, time_from_last_punch, tool_capabilities, direction, damage)
        creatura.basic_punch_func(self, puncher, time_from_last_punch, tool_capabilities, direction, damage)
        local name = puncher:is_player() and puncher:get_player_name()
        if name then
            if self.owner
            and name == self.owner then
                return
            elseif not homo_sapiens.is_value_in_table(self.enemies, name) then
                table.insert(self.enemies, name)
                if #self.enemies > 15 then
                    table.remove(self.enemies, 1)
                end
                self.enemies = self:memorize("enemies", self.enemies)
            else
                table.remove(self.enemies, 1)
                table.insert(self.enemies, name)
                self.enemies = self:memorize("enemies", self.enemies)
            end
        end
        self._target = puncher
    end,
	on_rightclick = function(self, clicker)
		if not clicker:is_player() then return end
		local name = clicker:get_player_name()
		local passive = true
		if homo_sapiens.is_value_in_table(self.enemies, name) then passive = false end
		if animalia.feed(self, clicker, passive, passive) then
			return
		end
		if animalia.set_nametag(self, clicker) then
			return
		end
		if self.owner
		and name == self.owner
		and clicker:get_player_control().sneak then
			local order = self.order
			if order == "wander" then
				minetest.chat_send_player(name, self.name.." is following")
				self.order = "follow"
				self:initiate_utility("animalia:follow_player", self, clicker, true)
				self:set_utility_score(0.7)
			elseif order == "follow" then
				minetest.chat_send_player(name, self.name.." is sitting")
				self.order = "sit"
				self:initiate_utility("animalia:stay", self)
				self:set_utility_score(0.5)
			else
				minetest.chat_send_player(name, self.name.." is wandering")
				self.order = "wander"
				self:set_utility_score(0)
			end
			self:memorize("order", self.order)
		end
	end,
    deactivate_func = function(self)
        if self.name then 
            self:memorize('name', self.name)
        end
		if self.uuid then
			self:memorize('uuid', self.uuid)
		end
        if self.owner then
            for i, object in ipairs(animalia.pets[self.owner] or {}) do
                if object == self.object then
                    animalia.pets[self.owner][i] = nil
                end
            end
        end
        if self.enemies
        and self.enemies[1]
        and self.memorize then
            self.enemies[1] = nil
            self.enemies = self:memorize("enemies", self.enemies)
        end
    end
})
creatura.register_spawn_egg("homo_sapiens:human", "e3c0a3" ,"3d2614")
creatura.register_abm_spawn("homo_sapiens:human", {
	chance = 20000,
	min_height = 0,
	max_height = 1024,
	min_group = 1,
	max_group = 2,
	biomes = animalia.registered_biome_groups["common"].biomes,
	nodes = {"group:soil"},
	neighbors = {"air", "group:grass", "group:flora"}
})

dofile(homo_sapiens.modpath..'/bones.lua')